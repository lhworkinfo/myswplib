import Foundation

public struct MySWPLib {
    
    public init(){}
    
    public func getHTML() -> String{
        do {
            //获取HTML源码（先获取第一页），此方法获取的是PC版的源码并不是移动端的
            let str = try String(contentsOf:URL.init(string: "https://www.google.co.jp/")!)
            print(str)
            return str
        } catch {
            print(error)
        }
        return "error"
    }
    
}
